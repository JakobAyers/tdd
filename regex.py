def extract_polynomial(polystr):
    """
    Create a list of 2-tuples of coefficents and exponents of a polynomial

      >>> extract_polynomial('3x^4+7x^2')
      [(3, 4), (7, 2)]
      >>> extract_polynomial('x^9-3x^4-x+42')
      [(1, 9), (-3, 4), (-1, 1), (42, 0)]
    """
    import re
    regex = re.compile(r"(?:\dx\^\d|(x\^\d))")
    return regex.findall(polystr)

if __name__ == '__main__':
    import doctest
    doctest.testmod()
